package ui.table
{
	
	import mx.controls.DataGrid;
	import ui.table.TableSkin;
	
	public class Table extends DataGrid
	{
		public var skinClass:Class;
		
		public function Table()
		{
			super();
			this.setStyle('skinClass', TableSkin);
		}
	}
}